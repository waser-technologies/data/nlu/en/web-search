from typing import Any, Text, Dict, List

import os
import glob
import yaml

from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet, FollowupAction

from .searcher import Searcher

# https://rasa.com/docs/rasa/custom-actions
# https://rasa.com/docs/action-server/sdk-actions


#
# This file is part of a loose module called actions.
#
# Once this domain installed, this module will take the name of the domain.
#
# So don't use `from actions.my_file import stuff` but rather `from .my_file import stuff`.
#

class ValidateSearchForm(FormValidationAction):

    SE_LIST = []

    def __init__(self):
        super().__init__()
        self.SE_LIST = [
            "google",
            "bing",
            "duckduckgo",
            "ask"
        ]

    def name(self) -> Text:
        return "validate_search_form"

    def validate_query(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]
    ) -> List[Dict[Text, Any]]:
        return {'query': slot_value}

    def validate_search_engine(
        self,
        slot_value: Any,
        dispatcher: CollectingDispatcher,
        tracker: Tracker,
        domain: Dict[Text, Any]
    ) -> List[Dict[Text, Any]]:
        if slot_value.lower() not in self.SE_LIST:
            str_list_se = '\n- '.join(self.SE_LIST)
            msg = f"Implemented engines are:\n- {str_list_se}"
            dispatcher.utter_message(text=msg)
            return {'search_engine': None}
        return {'search_engine': slot_value}

class ActionSummarizeSearchWeb(Action):
    
    def __init__(self):
        super().__init__()
        self.user = os.environ.get('USERNAME', 'user').lower()
        self.searcher = Searcher(self.user)

    def name(self) -> Text:
        return "action_search_web_values"

    async def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        query = tracker.get_slot("query")
        engine = tracker.get_slot("search_engine")
        self.searcher.answer(query, engine)
        return [SlotSet("query", None)]
