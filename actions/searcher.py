import re
import torch
import requests
import json
import trafilatura
import logging
import threading
import random

from num2words import num2words
from hanapin import Google, Bing, DuckDuckGo, Ask
from transformers import PegasusForConditionalGeneration, PegasusTokenizer
from transformers import pipeline
from quantulum3 import parser

logger = logging.Logger("Searcher")

# Init RoBERTa
answerer_model_name = "deepset/roberta-base-squad2"
nlp = pipeline('question-answering', model=answerer_model_name, tokenizer=answerer_model_name, framework='pt')
# Init Pegasus
paraphraser_model_name = "tuner007/pegasus_paraphrase"
torch_device = "cuda" if torch.cuda.is_available() else "cpu"
paraphraser_tokenizer = PegasusTokenizer.from_pretrained(paraphraser_model_name)
paraphraser_model = PegasusForConditionalGeneration.from_pretrained(paraphraser_model_name).to(torch_device)

def please_wait_for_search():
    return random.choice([
        "Finding out. Please wait this will take some time.",
        "Looking up an answer. Please be patient while I search this for you.",
        "Give me a minute to search",
        "You can count on me chief. I report back to you once I have an answer."
        "Let me look this up. I need a second."
    ])

def not_enough_context():
    return random.choice([
        "It seems I couldn't find enough context to answer this query.",
        "I have not been able to gather enough material to answer this for you."
        "I'm sorry but there is just not enough data."
    ])

def answer_has_been_found():
    return random.choice([
        "I think I found the answer.",
        f"I belive I know the answer.",
        f"I think I can answer this query.",
        f"Sorry to disturb but I have found the answer you were looking for."
    ])

def sorry_no_answer():
    return random.choice([
        "I'm really sorry it took so long to tell you that I couldn't find an answer for you."
        "I'm sorry but I couldn't find the answer.",
        "Sorry, I couldn't find anything."
    ])

def assistant_say(user, text=None, host="localhost", port="5068"):
    t = text #or get_nlu_response_from(response)
    if t:
        logger.info(f"Saying: {t}")
        json_data = {'text': t}
        r = requests.post(f"http://{host}:{port}/api/v1/say/{user}/", json=json_data)
        
    return t

class BackgroundSearch(threading.Thread):

    query, context = None, None

    def __init__(self, user):
        super().__init__()
        self.daemon = True
        self.user = user
               
        self.query, self.context = None, None

    def run(self, *args, **kwargs):
        self.get_paraphrase_answer_from_context(self.query, self.context)
    
    def get_paraphrase_answer_from_context(self, query, context):
        response = self.get_answer_from_context(query, context)
        if response:
            _r = self.expand_units(response)
            response = _r
            spoken_response = self.get_response(answer_has_been_found())[0]
            assistant_say(self.user, text=spoken_response)
            logger.debug("Found answer.")
            spoken_response = None
            for paraphrase in self.get_response(f"{query}\n{response}", num_return_sequences=5):
                if response in paraphrase:
                    spoken_response = paraphrase
                    break
            if spoken_response:
                spoken_response = self.convert_num2words(spoken_response).capitalize()
                assistant_say(self.user, text=spoken_response)
                logger.debug("Could produce answer")
            else:
                spoken_response = self.convert_num2words(response).capitalize()
                assistant_say(self.user, text=spoken_response)
                logger.debug("Couldn't produce answer")
        else:
            logger.warning("Didn't found answer.")
            spoken_response = self.get_response(sorry_no_answer())[0]
            assistant_say(self.user, text=spoken_response)
        return spoken_response

    def expand_units(self, query):
        return parser.inline_parse_and_expand(query)

    def fetch_floats_from_str(self, text):
        l = []
        for t in text.split():
            try:
                l.append(float(t))
            except ValueError:
                pass
        return l

    def convert_num2words(self, sentences: str, language="en"):
        _sentences = sentences
        if re.search(r"[0-9]", _sentences) is not None:
            nums = self.fetch_floats_from_str(_sentences)
            for num in nums:
                n = str(num)
                w = num2words(num, lang=language)
                print(f"Found number: {n}")
                print(f"Replacing it with: {w}")
                _sentences = _sentences.replace(n, w)
                _sentences = _sentences.replace(str(int(num)), w)
        print(f"Converted {_sentences=}")
        return _sentences
    
    def get_response(self, input_text, num_return_sequences=1, num_beams=10):
        batch = paraphraser_tokenizer([input_text],truncation=True,padding='longest',max_length=60, return_tensors="pt").to(torch_device)
        translated = paraphraser_model.generate(**batch,max_length=60,num_beams=num_beams, num_return_sequences=num_return_sequences, temperature=1.5)
        tgt_text = paraphraser_tokenizer.batch_decode(translated, skip_special_tokens=True)
        return tgt_text
    
    def get_answer_from_context(self, question, context):
        # Returns an answer from a question and a piece of context
        # TODO: use confidence score to define a threshold
        print("Search context")
        print(context)
        if context and question:
            QA_input = {
                'question': question,
                'context': context
            }
            answer = nlp(QA_input)
            return answer.get('answer', None)
        else:
            return None

class Searcher:

    def __init__(self, user):
        super().__init__()
        
        self.user = user
        #self.back_search = BackgroundSearch(self.user)
        #self.back_search.daemon = True
        logger.debug(f"Init searcher for user {user}.")
    
    def answer(self, query, engine):
        logger.debug(f"Answering {query}")
        please_wait = BackgroundSearch(self.user).get_response(please_wait_for_search())[0]
        assistant_say(self.user, text=please_wait)
        context = self.get_webseach_summary(query, engine) #Must be runned in the main thread! (trafilatura)
        if context:
            logger.debug("Found context")
            back_search = BackgroundSearch(self.user)
            back_search.query = query
            back_search.context = context
            back_search.start()
        else:
            logger.warning("Didn't found context")
            spoken_response = BackgroundSearch(self.user).get_response(not_enough_context())[0]
            assistant_say(self.user, text=spoken_response)
            return spoken_response

    
    def get_webseach_summary(self, query, engine="google", n=3, qs=10):
        # TODO: Use more context
        answer = None
        #if len(query) >= qs:
        #    q = self.get_summary(query)
        ##else:
        q = query
        # get list of result for query on engine
        result_list = self.get_list_results_with_engine(engine, q)
        if not result_list:
            return None
        elif len(result_list) < n:
            n = len(result_list)
        # Get context
        context = ""
        for u in result_list[:n-1]:
            c = self.get_clean_content_from_url(u)
            if c:
                context += c + "\n"
        # Get answer
        return context
    
    def get_list_results_with_engine(self, engine, query):
        # TODO: Make more robust against netwok issues
        list_webpages = []
        # Find some relevant links about a seach query
        if engine == "google":
            s = Google(query=query)
        elif engine == "bing":
            s = Bing(query=query)
        elif engine == "duckduckgo":
            s = DuckDuckGo(query=query)
        elif engine == "ask":
            s = Ask(query=query)
        else:
            s = None

        if s:
            list_webpages = [i.get('link') for i in s.results() if i.get('link')]
        return list_webpages
    
    def get_clean_content_from_url(self, content_url):
        # Return the content of a given url
        # TODO: Make more robust agains small pages
        raw_content = trafilatura.fetch_url(content_url)
        if raw_content:
            # Exctract with precision but fast
            content_text = trafilatura.extract(raw_content, favor_precision=True, include_comments=False, include_tables=False, no_fallback=True, target_language='fr')
            if content_text:
                content_text = re.sub(r'(\w+)(\[\d+\])', r'\1 \2', content_text) # For scientific sources notation[1] -> notation [1] (add space to better tokenize) - you could also remove r`\2` from re.sub -
                # [1] IEEE, “How to format your references using the Proceedings of the IEEE citation style”, IEEE, https://paperpile.com/s/proceedings-of-the-ieee-citation-style/
            else:
                content_text = ""
        else:
            print("No raw content in:")
            print(raw_content)
            content_text = ""
        return content_text