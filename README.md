# Web Search: NLU Domain

Get summarized answers from the web.

> **DISCLAMER:** I'm in no way, shape or form in control of the produced output. Don't take any answer for granted.

```
->  search the web
What would you like to search?
->  When did Albert Einstein died?
18. april 1955
->  Who was Albert Einstein's mother?
Pauline einstein
->  How far is the moon?
238,855 miles
->  How tall is Joe Biden?
5 feet, 11.65 inches
->  Is Britney Spears in jail?
Britney spears is not in jail
->  Where is Switzerland?
West-central europe
->  How tall is mount everest?
8,848 metres
->  
```

## Requirements

You need to install the correct version of `torch` for your system.

Read [`pytorch` documentation](https://pytorch.org/TensorRT/tutorials/installation.html) for more information.

Have a look at the `requirements.txt` file to see all dependencies for this domain.

## Installation

Use the [Domain Management Tool](https://gitlab.com/waser-technologies/technologies/dmt) to effortlessly install, validate and train a new NLP model using this domain.

```zsh
dmt -V -T -L en -a https://gitlab.com/waser-technologies/data/nlu/en/web-search.git
```

## Usage

To test the model on this domain, use the `dmt` to serve the latest model.

```zsh
dmt -S --lang en
```
This will take a while to load.

Once you see:
> `INFO     root  - Rasa server is up and running.`

You can query the server in another terminal.

```zsh
curl -XPOST localhost:5005/webhooks/rest/webhook -s -d '{ "message": "how far is the moon", "sender": "user" }'
```

Or simply use [`Assistant`](https://gitlab.com/waser-technologies/technologies/assistant).

```zsh
assistant How far is the moon?
```

## How does it work?

This domain achives web search from queries that would be considered out-of-scope using two main components:
- a rasa search form to ask for:
    - a `query`
    - a `search_engine`
      (default `google` - `bing`, `duckduckgo`, `ask` -)
    
    When the user ask to search without providing those slots.
    
    This will trigger a custom search action
- a query intent to directly trigger:
    - the custom search action
    
    When the user ask what seams to be out-of-scope queries only a web search could answer.

If the query is too long, uses [`google/pegasus-xsum`](https://huggingface.co/google/pegasus-large) to summarize the query to search.

Takes the first three results and grabs the text content of it and uses [`deepset/roberta-base-squad2`](https://huggingface.co/deepset/roberta-base-squad2) to answer the query given the content of the search results' web pages as context.

## Is there pre-trained models of this domain?

Yes. I train and distribute models using all the [domains I've made public](https://gitlab.com/waser-technologies/data/nlu) for a particular language.

You can download the latest pre-trained models for english NLU under [Models/English/NLU](https://gitlab.com/waser-technologies/models/en/nlu).

## I have some questions!

> Why do you only take the first 3 results and not more?

It's a balance between the time it takes to answer and the accuracy of it.

> Why does it take so much time to answer?

You need to understand what the domain does here. It's summarizing and answering a complex question given a piece of relevant context. This is a computationnaly intensive process that is necessary to find the answer you are look for, from the web.

The domain is configured in such a way to provide an answer in less than 10 seconds (optimaly). If this is still too much, you can reduce the size of the context (from 3 to 1 page) and skip large query summarization altogether but it's still going to take some time to compute the answer.

> How can I improve the answers I'm getting?

If you want to privilege accuracy over speed, you can increase the size of the context (i.e. from 3 to 10 pages). Note that shooting too large for the context is going to produce bad results.

> Does this domain works with other languages than english?

No, domains are language specific. Futhermore, this domain relies on actions which use pre-trained models to produce answers from the web. Those models are language specific too and thus cannot be used to answer queries in any other language.

## Contributions

This domain was jumpstarted using the [NLP Domain Template](https://gitlab.com/waser-technologies/cookiecutters/nlu-domain-template).

```zsh
dmt --create
```